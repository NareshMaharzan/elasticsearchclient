# ElasticSearch client library
 
ElasticSearch client library with amazon elasticsearch PHP signing handler. This library is targeted for laravel but can also be used with other frameworks.

## Installation

Require package in composer.json file.
```json
    "require":{
        "naresh/elasticsearchclient": "1.0.*",
    }

```
And run composer update to update the dependency.

## Configuration (Laravel Only)

__NOTE: You do not have to add ServerProvider and Facade if you are using Laravel 5.5 package autodiscovery.__

* Add the ServiceProvider to the providers array in ___config/app.php___

    `Naresh\ElasticSearchClient\ServiceProvider::class`

* Add EsClient Facade in ___config/app.php___

    `'EsClient' => Naresh\ElasticSearchClient\Facade\EsClient::class,`
    
## Usage
You can get the new Elasticsearch instance in two ways

1) With `EsClient` Facade (Laravel Only)
    * `EsClient::getClient($options)`
2) With new instance of `Naresh\ElasticSearchClient\ElasticSearchClient`
    * `new ElasticSearchClient($options)`
    
### $options    
```php
 [
    'hosts' => [],
    'aws_host' => false,
    'access_key' => '',
    'secret_key' => '',
    'region' => ''
];
```
* hosts (__array__)
    * Elasticsearch host(s)
        
* aws_host (__boolean__)
    * Boolean flag to specify if the hostname is ___AWS Elasticsearch service___, however if your host has `amazonaws` in its URL then you dont have to add this flag, this library will assume it as ___AWS Elasticsearch service___
        
* access_key(__string__ only needed if host is ___AWS Elasticsearch service___)
    * ___AWS Elasticsearch service___ access key 
        
* secret_key(__string__ only needed if host is ___AWS Elasticsearch service___)
    * ___AWS Elasticsearch service___ secret key 
        
* region(__string__ only needed if host is ___AWS Elasticsearch service___)
    * ___AWS Elasticsearch service___ region


### Run tests

Run unit test with `phpunit` following command.

```php
    vendor/bin/phpunit -v
```

It will generate test coverage reports in `reports` folder