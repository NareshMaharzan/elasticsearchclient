<?php

namespace Naresh\ElasticSearchClient\Tests\Unit;

use Aws\Credentials\Credentials;
use Naresh\ElasticSearchClient\AwsRequestHandler;
use Naresh\ElasticSearchClient\Tests\TestCase;
use Aws\Signature\SignatureV4;
use Closure;
use Faker\Factory;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\Ring\Future\CompletedFutureArray;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriInterface;


/**
 * Class AwsRequestHandlerTest
 * @package Naresh\ElasticSearchClient\Tests\Unit
 */
class AwsRequestHandlerTest extends TestCase
{

    public function testSetCredentials()
    {
        $obj = new AwsRequestHandler();
        $actual = $obj->setCredentials('test', 'test');
        $this->assertInstanceOf(AwsRequestHandler::class, $actual);
    }

    public function testGetCredentials()
    {
        $obj = new AwsRequestHandler();
        $actual = $obj->getCredentials();
        $this->assertNull($actual);

        $obj->setCredentials('test', 'test');
        $actual = $obj->getCredentials();
        $this->assertInstanceOf(Credentials::class, $actual);
    }

    public function testSetSignature()
    {
        $obj = new AwsRequestHandler();
        $actual = $obj->setSignature('ap-southeast-2');
        $this->assertInstanceOf(AwsRequestHandler::class, $actual);
    }

    public function testGetSignature()
    {
        $obj = new AwsRequestHandler();
        $actual = $obj->getSignature();
        $this->assertNull($actual);

        $obj->setSignature('ap-southeast-2');
        $actual = $obj->getSignature();
        $this->assertInstanceOf(SignatureV4::class, $actual);
    }

    public function testGetHandler()
    {
        $handler = function () {
            return \GuzzleHttp\Promise\promise_for(new class implements \Psr\Http\Message\ResponseInterface
            {

                /**
                 * Retrieves the HTTP protocol version as a string.
                 *
                 * The string MUST contain only the HTTP version number (e.g., "1.1", "1.0").
                 *
                 * @return string HTTP protocol version.
                 */
                public function getProtocolVersion()
                {
                    // TODO: Implement getProtocolVersion() method.
                }

                /**
                 * Return an instance with the specified HTTP protocol version.
                 *
                 * The version string MUST contain only the HTTP version number (e.g.,
                 * "1.1", "1.0").
                 *
                 * This method MUST be implemented in such a way as to retain the
                 * immutability of the message, and MUST return an instance that has the
                 * new protocol version.
                 *
                 * @param string $version HTTP protocol version
                 * @return static
                 */
                public function withProtocolVersion($version)
                {
                    // TODO: Implement withProtocolVersion() method.
                }

                /**
                 * Retrieves all message header values.
                 *
                 * The keys represent the header name as it will be sent over the wire, and
                 * each value is an array of strings associated with the header.
                 *
                 *     // Represent the headers as a string
                 *     foreach ($message->getHeaders() as $name => $values) {
                 *         echo $name . ": " . implode(", ", $values);
                 *     }
                 *
                 *     // Emit headers iteratively:
                 *     foreach ($message->getHeaders() as $name => $values) {
                 *         foreach ($values as $value) {
                 *             header(sprintf('%s: %s', $name, $value), false);
                 *         }
                 *     }
                 *
                 * While header names are not case-sensitive, getHeaders() will preserve the
                 * exact case in which headers were originally specified.
                 *
                 * @return string[][] Returns an associative array of the message's headers. Each
                 *     key MUST be a header name, and each value MUST be an array of strings
                 *     for that header.
                 */
                public function getHeaders()
                {
                    // TODO: Implement getHeaders() method.
                }

                /**
                 * Checks if a header exists by the given case-insensitive name.
                 *
                 * @param string $name Case-insensitive header field name.
                 * @return bool Returns true if any header names match the given header
                 *     name using a case-insensitive string comparison. Returns false if
                 *     no matching header name is found in the message.
                 */
                public function hasHeader($name)
                {
                    // TODO: Implement hasHeader() method.
                }

                /**
                 * Retrieves a message header value by the given case-insensitive name.
                 *
                 * This method returns an array of all the header values of the given
                 * case-insensitive header name.
                 *
                 * If the header does not appear in the message, this method MUST return an
                 * empty array.
                 *
                 * @param string $name Case-insensitive header field name.
                 * @return string[] An array of string values as provided for the given
                 *    header. If the header does not appear in the message, this method MUST
                 *    return an empty array.
                 */
                public function getHeader($name)
                {
                    // TODO: Implement getHeader() method.
                }

                /**
                 * Retrieves a comma-separated string of the values for a single header.
                 *
                 * This method returns all of the header values of the given
                 * case-insensitive header name as a string concatenated together using
                 * a comma.
                 *
                 * NOTE: Not all header values may be appropriately represented using
                 * comma concatenation. For such headers, use getHeader() instead
                 * and supply your own delimiter when concatenating.
                 *
                 * If the header does not appear in the message, this method MUST return
                 * an empty string.
                 *
                 * @param string $name Case-insensitive header field name.
                 * @return string A string of values as provided for the given header
                 *    concatenated together using a comma. If the header does not appear in
                 *    the message, this method MUST return an empty string.
                 */
                public function getHeaderLine($name)
                {
                    // TODO: Implement getHeaderLine() method.
                }

                /**
                 * Return an instance with the provided value replacing the specified header.
                 *
                 * While header names are case-insensitive, the casing of the header will
                 * be preserved by this function, and returned from getHeaders().
                 *
                 * This method MUST be implemented in such a way as to retain the
                 * immutability of the message, and MUST return an instance that has the
                 * new and/or updated header and value.
                 *
                 * @param string $name Case-insensitive header field name.
                 * @param string|string[] $value Header value(s).
                 * @return static
                 * @throws \InvalidArgumentException for invalid header names or values.
                 */
                public function withHeader($name, $value)
                {
                    // TODO: Implement withHeader() method.
                }

                /**
                 * Return an instance with the specified header appended with the given value.
                 *
                 * Existing values for the specified header will be maintained. The new
                 * value(s) will be appended to the existing list. If the header did not
                 * exist previously, it will be added.
                 *
                 * This method MUST be implemented in such a way as to retain the
                 * immutability of the message, and MUST return an instance that has the
                 * new header and/or value.
                 *
                 * @param string $name Case-insensitive header field name to add.
                 * @param string|string[] $value Header value(s).
                 * @return static
                 * @throws \InvalidArgumentException for invalid header names or values.
                 */
                public function withAddedHeader($name, $value)
                {
                    // TODO: Implement withAddedHeader() method.
                }

                /**
                 * Return an instance without the specified header.
                 *
                 * Header resolution MUST be done without case-sensitivity.
                 *
                 * This method MUST be implemented in such a way as to retain the
                 * immutability of the message, and MUST return an instance that removes
                 * the named header.
                 *
                 * @param string $name Case-insensitive header field name to remove.
                 * @return static
                 */
                public function withoutHeader($name)
                {
                    // TODO: Implement withoutHeader() method.
                }

                /**
                 * Gets the body of the message.
                 *
                 * @return StreamInterface Returns the body as a stream.
                 */
                public function getBody()
                {
                    return $this;
                }

                public function detach()
                {
                    
                }

                /**
                 * Return an instance with the specified message body.
                 *
                 * The body MUST be a StreamInterface object.
                 *
                 * This method MUST be implemented in such a way as to retain the
                 * immutability of the message, and MUST return a new instance that has the
                 * new body stream.
                 *
                 * @param StreamInterface $body Body.
                 * @return static
                 * @throws \InvalidArgumentException When the body is not valid.
                 */
                public function withBody(StreamInterface $body)
                {
                    // TODO: Implement withBody() method.
                }

                /**
                 * Gets the response status code.
                 *
                 * The status code is a 3-digit integer result code of the server's attempt
                 * to understand and satisfy the request.
                 *
                 * @return int Status code.
                 */
                public function getStatusCode()
                {
                    // TODO: Implement getStatusCode() method.
                }

                /**
                 * Return an instance with the specified status code and, optionally, reason phrase.
                 *
                 * If no reason phrase is specified, implementations MAY choose to default
                 * to the RFC 7231 or IANA recommended reason phrase for the response's
                 * status code.
                 *
                 * This method MUST be implemented in such a way as to retain the
                 * immutability of the message, and MUST return an instance that has the
                 * updated status and reason phrase.
                 *
                 * @link http://tools.ietf.org/html/rfc7231#section-6
                 * @link http://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
                 * @param int $code The 3-digit integer result code to set.
                 * @param string $reasonPhrase The reason phrase to use with the
                 *     provided status code; if none is provided, implementations MAY
                 *     use the defaults as suggested in the HTTP specification.
                 * @return static
                 * @throws \InvalidArgumentException For invalid status code arguments.
                 */
                public function withStatus($code, $reasonPhrase = '')
                {
                    // TODO: Implement withStatus() method.
                }

                /**
                 * Gets the response reason phrase associated with the status code.
                 *
                 * Because a reason phrase is not a required element in a response
                 * status line, the reason phrase value MAY be null. Implementations MAY
                 * choose to return the default RFC 7231 recommended reason phrase (or those
                 * listed in the IANA HTTP Status Code Registry) for the response's
                 * status code.
                 *
                 * @link http://tools.ietf.org/html/rfc7231#section-6
                 * @link http://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
                 * @return string Reason phrase; must return an empty string if none present.
                 */
                public function getReasonPhrase()
                {
                    // TODO: Implement getReasonPhrase() method.
                }
            });
        };
        $obj = new AwsRequestHandler();
        $actual = $obj->getHandler($handler);
        $faker = Factory::create();
        $request['headers'] = [];
        $request['headers']['host'][0] = $faker->url;
        $request['http_method'] = $faker->randomElement(["GET", "POST"]);
        $request['scheme'] = 'http';
        $request['uri'] = 'test';
        $request['body'] = json_encode([]);
        $obj->setSignature('ap-southeast-2');
        $obj->setCredentials('test', 'test');
        $actual($request);

        $this->assertInstanceOf(Closure::class, $actual);
    }

    public function testGetRequest()
    {
        $faker = Factory::create();
        $request['headers'] = [];
        $request['headers']['host'][0] = $faker->url;
        $request['http_method'] = $faker->randomElement(["GET", "POST"]);
        $request['scheme'] = 'http';
        $request['uri'] = 'test';
        $request['body'] = json_encode([]);
        $obj = new AwsRequestHandler();
        $actual = $obj->getRequest($request);
        $this->assertInstanceOf(Request::class, $actual);
    }

    public function testGetUri()
    {
        $obj = new AwsRequestHandler();
        $actual = $obj->getUri('/test', 'http', 'somewhere.com');
        $this->assertInstanceOf(Uri::class, $actual);
    }

    public function testSignRequest()
    {
        $request = new Request('GET', '/test');
        $obj = new AwsRequestHandler();
        $obj->setSignature('ap-southeast-2');
        $obj->setCredentials('test', 'test');
        $actual = $obj->signRequest($request);
        $this->assertInstanceOf(Request::class, $actual);
    }

    public function testSendRequestToAws()
    {
        $request = new Request('GET', '/test');
        $obj = new AwsRequestHandler();
        $obj->setSignature('ap-southeast-2');
        $obj->setCredentials('test', 'test');
        $signedRequest = $obj->signRequest($request);
        $handler = function () {
            return \GuzzleHttp\Promise\promise_for(new class implements \Psr\Http\Message\ResponseInterface
            {

                /**
                 * Retrieves the HTTP protocol version as a string.
                 *
                 * The string MUST contain only the HTTP version number (e.g., "1.1", "1.0").
                 *
                 * @return string HTTP protocol version.
                 */
                public function getProtocolVersion()
                {
                    // TODO: Implement getProtocolVersion() method.
                }

                /**
                 * Return an instance with the specified HTTP protocol version.
                 *
                 * The version string MUST contain only the HTTP version number (e.g.,
                 * "1.1", "1.0").
                 *
                 * This method MUST be implemented in such a way as to retain the
                 * immutability of the message, and MUST return an instance that has the
                 * new protocol version.
                 *
                 * @param string $version HTTP protocol version
                 * @return static
                 */
                public function withProtocolVersion($version)
                {
                    // TODO: Implement withProtocolVersion() method.
                }

                /**
                 * Retrieves all message header values.
                 *
                 * The keys represent the header name as it will be sent over the wire, and
                 * each value is an array of strings associated with the header.
                 *
                 *     // Represent the headers as a string
                 *     foreach ($message->getHeaders() as $name => $values) {
                 *         echo $name . ": " . implode(", ", $values);
                 *     }
                 *
                 *     // Emit headers iteratively:
                 *     foreach ($message->getHeaders() as $name => $values) {
                 *         foreach ($values as $value) {
                 *             header(sprintf('%s: %s', $name, $value), false);
                 *         }
                 *     }
                 *
                 * While header names are not case-sensitive, getHeaders() will preserve the
                 * exact case in which headers were originally specified.
                 *
                 * @return string[][] Returns an associative array of the message's headers. Each
                 *     key MUST be a header name, and each value MUST be an array of strings
                 *     for that header.
                 */
                public function getHeaders()
                {
                    // TODO: Implement getHeaders() method.
                }

                /**
                 * Checks if a header exists by the given case-insensitive name.
                 *
                 * @param string $name Case-insensitive header field name.
                 * @return bool Returns true if any header names match the given header
                 *     name using a case-insensitive string comparison. Returns false if
                 *     no matching header name is found in the message.
                 */
                public function hasHeader($name)
                {
                    // TODO: Implement hasHeader() method.
                }

                /**
                 * Retrieves a message header value by the given case-insensitive name.
                 *
                 * This method returns an array of all the header values of the given
                 * case-insensitive header name.
                 *
                 * If the header does not appear in the message, this method MUST return an
                 * empty array.
                 *
                 * @param string $name Case-insensitive header field name.
                 * @return string[] An array of string values as provided for the given
                 *    header. If the header does not appear in the message, this method MUST
                 *    return an empty array.
                 */
                public function getHeader($name)
                {
                    // TODO: Implement getHeader() method.
                }

                /**
                 * Retrieves a comma-separated string of the values for a single header.
                 *
                 * This method returns all of the header values of the given
                 * case-insensitive header name as a string concatenated together using
                 * a comma.
                 *
                 * NOTE: Not all header values may be appropriately represented using
                 * comma concatenation. For such headers, use getHeader() instead
                 * and supply your own delimiter when concatenating.
                 *
                 * If the header does not appear in the message, this method MUST return
                 * an empty string.
                 *
                 * @param string $name Case-insensitive header field name.
                 * @return string A string of values as provided for the given header
                 *    concatenated together using a comma. If the header does not appear in
                 *    the message, this method MUST return an empty string.
                 */
                public function getHeaderLine($name)
                {
                    // TODO: Implement getHeaderLine() method.
                }

                /**
                 * Return an instance with the provided value replacing the specified header.
                 *
                 * While header names are case-insensitive, the casing of the header will
                 * be preserved by this function, and returned from getHeaders().
                 *
                 * This method MUST be implemented in such a way as to retain the
                 * immutability of the message, and MUST return an instance that has the
                 * new and/or updated header and value.
                 *
                 * @param string $name Case-insensitive header field name.
                 * @param string|string[] $value Header value(s).
                 * @return static
                 * @throws \InvalidArgumentException for invalid header names or values.
                 */
                public function withHeader($name, $value)
                {
                    // TODO: Implement withHeader() method.
                }

                /**
                 * Return an instance with the specified header appended with the given value.
                 *
                 * Existing values for the specified header will be maintained. The new
                 * value(s) will be appended to the existing list. If the header did not
                 * exist previously, it will be added.
                 *
                 * This method MUST be implemented in such a way as to retain the
                 * immutability of the message, and MUST return an instance that has the
                 * new header and/or value.
                 *
                 * @param string $name Case-insensitive header field name to add.
                 * @param string|string[] $value Header value(s).
                 * @return static
                 * @throws \InvalidArgumentException for invalid header names or values.
                 */
                public function withAddedHeader($name, $value)
                {
                    // TODO: Implement withAddedHeader() method.
                }

                /**
                 * Return an instance without the specified header.
                 *
                 * Header resolution MUST be done without case-sensitivity.
                 *
                 * This method MUST be implemented in such a way as to retain the
                 * immutability of the message, and MUST return an instance that removes
                 * the named header.
                 *
                 * @param string $name Case-insensitive header field name to remove.
                 * @return static
                 */
                public function withoutHeader($name)
                {
                    // TODO: Implement withoutHeader() method.
                }

                /**
                 * Gets the body of the message.
                 *
                 * @return StreamInterface Returns the body as a stream.
                 */
                public function getBody()
                {
                    // TODO: Implement getBody() method.
                }

                /**
                 * Return an instance with the specified message body.
                 *
                 * The body MUST be a StreamInterface object.
                 *
                 * This method MUST be implemented in such a way as to retain the
                 * immutability of the message, and MUST return a new instance that has the
                 * new body stream.
                 *
                 * @param StreamInterface $body Body.
                 * @return static
                 * @throws \InvalidArgumentException When the body is not valid.
                 */
                public function withBody(StreamInterface $body)
                {
                    // TODO: Implement withBody() method.
                }

                /**
                 * Gets the response status code.
                 *
                 * The status code is a 3-digit integer result code of the server's attempt
                 * to understand and satisfy the request.
                 *
                 * @return int Status code.
                 */
                public function getStatusCode()
                {
                    // TODO: Implement getStatusCode() method.
                }

                /**
                 * Return an instance with the specified status code and, optionally, reason phrase.
                 *
                 * If no reason phrase is specified, implementations MAY choose to default
                 * to the RFC 7231 or IANA recommended reason phrase for the response's
                 * status code.
                 *
                 * This method MUST be implemented in such a way as to retain the
                 * immutability of the message, and MUST return an instance that has the
                 * updated status and reason phrase.
                 *
                 * @link http://tools.ietf.org/html/rfc7231#section-6
                 * @link http://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
                 * @param int $code The 3-digit integer result code to set.
                 * @param string $reasonPhrase The reason phrase to use with the
                 *     provided status code; if none is provided, implementations MAY
                 *     use the defaults as suggested in the HTTP specification.
                 * @return static
                 * @throws \InvalidArgumentException For invalid status code arguments.
                 */
                public function withStatus($code, $reasonPhrase = '')
                {
                    // TODO: Implement withStatus() method.
                }

                /**
                 * Gets the response reason phrase associated with the status code.
                 *
                 * Because a reason phrase is not a required element in a response
                 * status line, the reason phrase value MAY be null. Implementations MAY
                 * choose to return the default RFC 7231 recommended reason phrase (or those
                 * listed in the IANA HTTP Status Code Registry) for the response's
                 * status code.
                 *
                 * @link http://tools.ietf.org/html/rfc7231#section-6
                 * @link http://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
                 * @return string Reason phrase; must return an empty string if none present.
                 */
                public function getReasonPhrase()
                {
                    // TODO: Implement getReasonPhrase() method.
                }
            });
        };
        $actual = $obj->sendRequestToAws($handler, $signedRequest);
        $this->assertNotNull($actual);
    }

    public function testGetRingPhpResponse()
    {
        $request = new class implements RequestInterface
        {
            /**
             * Retrieves the HTTP protocol version as a string.
             *
             * The string MUST contain only the HTTP version number (e.g., "1.1", "1.0").
             *
             * @return string HTTP protocol version.
             */
            public function getProtocolVersion()
            {
                // TODO: Implement getProtocolVersion() method.
            }

            /**
             * Return an instance with the specified HTTP protocol version.
             *
             * The version string MUST contain only the HTTP version number (e.g.,
             * "1.1", "1.0").
             *
             * This method MUST be implemented in such a way as to retain the
             * immutability of the message, and MUST return an instance that has the
             * new protocol version.
             *
             * @param string $version HTTP protocol version
             * @return static
             */
            public function withProtocolVersion($version)
            {
                // TODO: Implement withProtocolVersion() method.
            }

            /**
             * Retrieves all message header values.
             *
             * The keys represent the header name as it will be sent over the wire, and
             * each value is an array of strings associated with the header.
             *
             *     // Represent the headers as a string
             *     foreach ($message->getHeaders() as $name => $values) {
             *         echo $name . ": " . implode(", ", $values);
             *     }
             *
             *     // Emit headers iteratively:
             *     foreach ($message->getHeaders() as $name => $values) {
             *         foreach ($values as $value) {
             *             header(sprintf('%s: %s', $name, $value), false);
             *         }
             *     }
             *
             * While header names are not case-sensitive, getHeaders() will preserve the
             * exact case in which headers were originally specified.
             *
             * @return string[][] Returns an associative array of the message's headers. Each
             *     key MUST be a header name, and each value MUST be an array of strings
             *     for that header.
             */
            public function getHeaders()
            {
                // TODO: Implement getHeaders() method.
            }

            /**
             * Checks if a header exists by the given case-insensitive name.
             *
             * @param string $name Case-insensitive header field name.
             * @return bool Returns true if any header names match the given header
             *     name using a case-insensitive string comparison. Returns false if
             *     no matching header name is found in the message.
             */
            public function hasHeader($name)
            {
                // TODO: Implement hasHeader() method.
            }

            /**
             * Retrieves a message header value by the given case-insensitive name.
             *
             * This method returns an array of all the header values of the given
             * case-insensitive header name.
             *
             * If the header does not appear in the message, this method MUST return an
             * empty array.
             *
             * @param string $name Case-insensitive header field name.
             * @return string[] An array of string values as provided for the given
             *    header. If the header does not appear in the message, this method MUST
             *    return an empty array.
             */
            public function getHeader($name)
            {
                // TODO: Implement getHeader() method.
            }

            /**
             * Retrieves a comma-separated string of the values for a single header.
             *
             * This method returns all of the header values of the given
             * case-insensitive header name as a string concatenated together using
             * a comma.
             *
             * NOTE: Not all header values may be appropriately represented using
             * comma concatenation. For such headers, use getHeader() instead
             * and supply your own delimiter when concatenating.
             *
             * If the header does not appear in the message, this method MUST return
             * an empty string.
             *
             * @param string $name Case-insensitive header field name.
             * @return string A string of values as provided for the given header
             *    concatenated together using a comma. If the header does not appear in
             *    the message, this method MUST return an empty string.
             */
            public function getHeaderLine($name)
            {
                // TODO: Implement getHeaderLine() method.
            }

            /**
             * Return an instance with the provided value replacing the specified header.
             *
             * While header names are case-insensitive, the casing of the header will
             * be preserved by this function, and returned from getHeaders().
             *
             * This method MUST be implemented in such a way as to retain the
             * immutability of the message, and MUST return an instance that has the
             * new and/or updated header and value.
             *
             * @param string $name Case-insensitive header field name.
             * @param string|string[] $value Header value(s).
             * @return static
             * @throws \InvalidArgumentException for invalid header names or values.
             */
            public function withHeader($name, $value)
            {
                // TODO: Implement withHeader() method.
            }

            /**
             * Return an instance with the specified header appended with the given value.
             *
             * Existing values for the specified header will be maintained. The new
             * value(s) will be appended to the existing list. If the header did not
             * exist previously, it will be added.
             *
             * This method MUST be implemented in such a way as to retain the
             * immutability of the message, and MUST return an instance that has the
             * new header and/or value.
             *
             * @param string $name Case-insensitive header field name to add.
             * @param string|string[] $value Header value(s).
             * @return static
             * @throws \InvalidArgumentException for invalid header names or values.
             */
            public function withAddedHeader($name, $value)
            {
                // TODO: Implement withAddedHeader() method.
            }

            /**
             * Return an instance without the specified header.
             *
             * Header resolution MUST be done without case-sensitivity.
             *
             * This method MUST be implemented in such a way as to retain the
             * immutability of the message, and MUST return an instance that removes
             * the named header.
             *
             * @param string $name Case-insensitive header field name to remove.
             * @return static
             */
            public function withoutHeader($name)
            {
                // TODO: Implement withoutHeader() method.
            }

            /**
             * Gets the body of the message.
             *
             * @return StreamInterface Returns the body as a stream.
             */
            public function getBody()
            {
                // TODO: Implement getBody() method.
            }

            /**
             * Return an instance with the specified message body.
             *
             * The body MUST be a StreamInterface object.
             *
             * This method MUST be implemented in such a way as to retain the
             * immutability of the message, and MUST return a new instance that has the
             * new body stream.
             *
             * @param StreamInterface $body Body.
             * @return static
             * @throws \InvalidArgumentException When the body is not valid.
             */
            public function withBody(StreamInterface $body)
            {
                // TODO: Implement withBody() method.
            }

            /**
             * Retrieves the message's request target.
             *
             * Retrieves the message's request-target either as it will appear (for
             * clients), as it appeared at request (for servers), or as it was
             * specified for the instance (see withRequestTarget()).
             *
             * In most cases, this will be the origin-form of the composed URI,
             * unless a value was provided to the concrete implementation (see
             * withRequestTarget() below).
             *
             * If no URI is available, and no request-target has been specifically
             * provided, this method MUST return the string "/".
             *
             * @return string
             */
            public function getRequestTarget()
            {
                // TODO: Implement getRequestTarget() method.
            }

            /**
             * Return an instance with the specific request-target.
             *
             * If the request needs a non-origin-form request-target — e.g., for
             * specifying an absolute-form, authority-form, or asterisk-form —
             * this method may be used to create an instance with the specified
             * request-target, verbatim.
             *
             * This method MUST be implemented in such a way as to retain the
             * immutability of the message, and MUST return an instance that has the
             * changed request target.
             *
             * @link http://tools.ietf.org/html/rfc7230#section-5.3 (for the various
             *     request-target forms allowed in request messages)
             * @param mixed $requestTarget
             * @return static
             */
            public function withRequestTarget($requestTarget)
            {
                // TODO: Implement withRequestTarget() method.
            }

            /**
             * Retrieves the HTTP method of the request.
             *
             * @return string Returns the request method.
             */
            public function getMethod()
            {
                // TODO: Implement getMethod() method.
            }

            /**
             * Return an instance with the provided HTTP method.
             *
             * While HTTP method names are typically all uppercase characters, HTTP
             * method names are case-sensitive and thus implementations SHOULD NOT
             * modify the given string.
             *
             * This method MUST be implemented in such a way as to retain the
             * immutability of the message, and MUST return an instance that has the
             * changed request method.
             *
             * @param string $method Case-sensitive method.
             * @return static
             * @throws \InvalidArgumentException for invalid HTTP methods.
             */
            public function withMethod($method)
            {
                // TODO: Implement withMethod() method.
            }

            /**
             * Retrieves the URI instance.
             *
             * This method MUST return a UriInterface instance.
             *
             * @link http://tools.ietf.org/html/rfc3986#section-4.3
             * @return UriInterface Returns a UriInterface instance
             *     representing the URI of the request.
             */
            public function getUri()
            {
                // TODO: Implement getUri() method.
            }

            /**
             * Returns an instance with the provided URI.
             *
             * This method MUST update the Host header of the returned request by
             * default if the URI contains a host component. If the URI does not
             * contain a host component, any pre-existing Host header MUST be carried
             * over to the returned request.
             *
             * You can opt-in to preserving the original state of the Host header by
             * setting `$preserveHost` to `true`. When `$preserveHost` is set to
             * `true`, this method interacts with the Host header in the following ways:
             *
             * - If the Host header is missing or empty, and the new URI contains
             *   a host component, this method MUST update the Host header in the returned
             *   request.
             * - If the Host header is missing or empty, and the new URI does not contain a
             *   host component, this method MUST NOT update the Host header in the returned
             *   request.
             * - If a Host header is present and non-empty, this method MUST NOT update
             *   the Host header in the returned request.
             *
             * This method MUST be implemented in such a way as to retain the
             * immutability of the message, and MUST return an instance that has the
             * new UriInterface instance.
             *
             * @link http://tools.ietf.org/html/rfc3986#section-4.3
             * @param UriInterface $uri New request URI to use.
             * @param bool $preserveHost Preserve the original state of the Host header.
             * @return static
             */
            public function withUri(UriInterface $uri, $preserveHost = false)
            {
                // TODO: Implement withUri() method.
            }
        };
        $response = new class implements ResponseInterface
        {
            /**
             * Retrieves the HTTP protocol version as a string.
             *
             * The string MUST contain only the HTTP version number (e.g., "1.1", "1.0").
             *
             * @return string HTTP protocol version.
             */
            public function getProtocolVersion()
            {
                // TODO: Implement getProtocolVersion() method.
            }

            /**
             * Return an instance with the specified HTTP protocol version.
             *
             * The version string MUST contain only the HTTP version number (e.g.,
             * "1.1", "1.0").
             *
             * This method MUST be implemented in such a way as to retain the
             * immutability of the message, and MUST return an instance that has the
             * new protocol version.
             *
             * @param string $version HTTP protocol version
             * @return static
             */
            public function withProtocolVersion($version)
            {
                // TODO: Implement withProtocolVersion() method.
            }

            /**
             * Retrieves all message header values.
             *
             * The keys represent the header name as it will be sent over the wire, and
             * each value is an array of strings associated with the header.
             *
             *     // Represent the headers as a string
             *     foreach ($message->getHeaders() as $name => $values) {
             *         echo $name . ": " . implode(", ", $values);
             *     }
             *
             *     // Emit headers iteratively:
             *     foreach ($message->getHeaders() as $name => $values) {
             *         foreach ($values as $value) {
             *             header(sprintf('%s: %s', $name, $value), false);
             *         }
             *     }
             *
             * While header names are not case-sensitive, getHeaders() will preserve the
             * exact case in which headers were originally specified.
             *
             * @return string[][] Returns an associative array of the message's headers. Each
             *     key MUST be a header name, and each value MUST be an array of strings
             *     for that header.
             */
            public function getHeaders()
            {
                // TODO: Implement getHeaders() method.
            }

            /**
             * Checks if a header exists by the given case-insensitive name.
             *
             * @param string $name Case-insensitive header field name.
             * @return bool Returns true if any header names match the given header
             *     name using a case-insensitive string comparison. Returns false if
             *     no matching header name is found in the message.
             */
            public function hasHeader($name)
            {
                // TODO: Implement hasHeader() method.
            }

            /**
             * Retrieves a message header value by the given case-insensitive name.
             *
             * This method returns an array of all the header values of the given
             * case-insensitive header name.
             *
             * If the header does not appear in the message, this method MUST return an
             * empty array.
             *
             * @param string $name Case-insensitive header field name.
             * @return string[] An array of string values as provided for the given
             *    header. If the header does not appear in the message, this method MUST
             *    return an empty array.
             */
            public function getHeader($name)
            {
                // TODO: Implement getHeader() method.
            }

            /**
             * Retrieves a comma-separated string of the values for a single header.
             *
             * This method returns all of the header values of the given
             * case-insensitive header name as a string concatenated together using
             * a comma.
             *
             * NOTE: Not all header values may be appropriately represented using
             * comma concatenation. For such headers, use getHeader() instead
             * and supply your own delimiter when concatenating.
             *
             * If the header does not appear in the message, this method MUST return
             * an empty string.
             *
             * @param string $name Case-insensitive header field name.
             * @return string A string of values as provided for the given header
             *    concatenated together using a comma. If the header does not appear in
             *    the message, this method MUST return an empty string.
             */
            public function getHeaderLine($name)
            {
                // TODO: Implement getHeaderLine() method.
            }

            /**
             * Return an instance with the provided value replacing the specified header.
             *
             * While header names are case-insensitive, the casing of the header will
             * be preserved by this function, and returned from getHeaders().
             *
             * This method MUST be implemented in such a way as to retain the
             * immutability of the message, and MUST return an instance that has the
             * new and/or updated header and value.
             *
             * @param string $name Case-insensitive header field name.
             * @param string|string[] $value Header value(s).
             * @return static
             * @throws \InvalidArgumentException for invalid header names or values.
             */
            public function withHeader($name, $value)
            {
                // TODO: Implement withHeader() method.
            }

            /**
             * Return an instance with the specified header appended with the given value.
             *
             * Existing values for the specified header will be maintained. The new
             * value(s) will be appended to the existing list. If the header did not
             * exist previously, it will be added.
             *
             * This method MUST be implemented in such a way as to retain the
             * immutability of the message, and MUST return an instance that has the
             * new header and/or value.
             *
             * @param string $name Case-insensitive header field name to add.
             * @param string|string[] $value Header value(s).
             * @return static
             * @throws \InvalidArgumentException for invalid header names or values.
             */
            public function withAddedHeader($name, $value)
            {
                // TODO: Implement withAddedHeader() method.
            }

            /**
             * Return an instance without the specified header.
             *
             * Header resolution MUST be done without case-sensitivity.
             *
             * This method MUST be implemented in such a way as to retain the
             * immutability of the message, and MUST return an instance that removes
             * the named header.
             *
             * @param string $name Case-insensitive header field name to remove.
             * @return static
             */
            public function withoutHeader($name)
            {
                // TODO: Implement withoutHeader() method.
            }

            /**
             * Gets the body of the message.
             *
             * @return StreamInterface Returns the body as a stream.
             */
            public function getBody()
            {
                return $this;
            }

            public function detach()
            {

            }

            /**
             * Return an instance with the specified message body.
             *
             * The body MUST be a StreamInterface object.
             *
             * This method MUST be implemented in such a way as to retain the
             * immutability of the message, and MUST return a new instance that has the
             * new body stream.
             *
             * @param StreamInterface $body Body.
             * @return static
             * @throws \InvalidArgumentException When the body is not valid.
             */
            public function withBody(StreamInterface $body)
            {
                // TODO: Implement withBody() method.
            }

            /**
             * Gets the response status code.
             *
             * The status code is a 3-digit integer result code of the server's attempt
             * to understand and satisfy the request.
             *
             * @return int Status code.
             */
            public function getStatusCode()
            {
                // TODO: Implement getStatusCode() method.
            }

            /**
             * Return an instance with the specified status code and, optionally, reason phrase.
             *
             * If no reason phrase is specified, implementations MAY choose to default
             * to the RFC 7231 or IANA recommended reason phrase for the response's
             * status code.
             *
             * This method MUST be implemented in such a way as to retain the
             * immutability of the message, and MUST return an instance that has the
             * updated status and reason phrase.
             *
             * @link http://tools.ietf.org/html/rfc7231#section-6
             * @link http://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
             * @param int $code The 3-digit integer result code to set.
             * @param string $reasonPhrase The reason phrase to use with the
             *     provided status code; if none is provided, implementations MAY
             *     use the defaults as suggested in the HTTP specification.
             * @return static
             * @throws \InvalidArgumentException For invalid status code arguments.
             */
            public function withStatus($code, $reasonPhrase = '')
            {
                // TODO: Implement withStatus() method.
            }

            /**
             * Gets the response reason phrase associated with the status code.
             *
             * Because a reason phrase is not a required element in a response
             * status line, the reason phrase value MAY be null. Implementations MAY
             * choose to return the default RFC 7231 recommended reason phrase (or those
             * listed in the IANA HTTP Status Code Registry) for the response's
             * status code.
             *
             * @link http://tools.ietf.org/html/rfc7231#section-6
             * @link http://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
             * @return string Reason phrase; must return an empty string if none present.
             */
            public function getReasonPhrase()
            {
                // TODO: Implement getReasonPhrase() method.
            }
        };
        $obj = new AwsRequestHandler();
        $actual = $obj->getRingPhpResponse($request, $response);
        $this->assertInstanceOf(CompletedFutureArray::class, $actual);

    }
}