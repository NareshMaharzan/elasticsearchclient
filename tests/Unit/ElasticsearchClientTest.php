<?php

namespace Naresh\ElasticSearchClient\Tests\Unit;

use Aws\Handler\GuzzleV6\GuzzleHandler;
use Naresh\ElasticSearchClient\AwsRequestHandler;
use Naresh\ElasticSearchClient\ElasticsearchClient;
use Naresh\ElasticSearchClient\Tests\TestCase;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;


/**
 * Class ElasticsearchClientTest
 * @package Unit
 */
class ElasticsearchClientTest extends TestCase
{

    public function testGetClientWithoutAwsEndpoint()
    {
        $options = [
            'hosts' => [
                'test.abc.com'
            ]
        ];
        $obj = new ElasticsearchClient();
        $actual = $obj->getClient($options);
        $this->assertInstanceOf(Client::class, $actual);
    }

    public function testGetClientWithAwsEndpoint()
    {
        $options = [
            'hosts' => [
                'test.abc.com'
            ],
            'aws_host' => true,
            'access_key' => 'KJHFI8342jhk',
            'secret_key' => 'FRD64adf321',
            'region' => 'ap-southeast-2',
        ];
        $obj = new ElasticsearchClient();
        $actual = $obj->getClient($options);
        $this->assertInstanceOf(Client::class, $actual);
    }

    public function testGetNormalClient()
    {
        $options = [
            'hosts' => [
                'test.abc.com'
            ],
            'aws_host' => true,
            'access_key' => 'KJHFI8342jhk',
            'secret_key' => 'FRD64adf321',
            'region' => 'ap-southeast-2',
        ];
        $obj = new ElasticsearchClient();
        $actual = $obj->getNormalClient($options);
        $this->assertInstanceOf(Client::class, $actual);
    }

    public function testGetAwsEsClient()
    {
        $options = [
            'hosts' => [
                'test.abc.com'
            ],
            'aws_host' => true,
            'access_key' => 'KJHFI8342jhk',
            'secret_key' => 'FRD64adf321',
            'region' => 'ap-southeast-2',
        ];
        $obj = new ElasticsearchClient();
        $actual = $obj->getAwsEsClient($options);
        $this->assertInstanceOf(Client::class, $actual);
    }

    public function testGetClientBuilder()
    {
        $obj = new ElasticsearchClient();
        $actual = $obj->getClientBuilder();
        $this->assertInstanceOf(ClientBuilder::class, $actual);
    }

    public function testGetAwsRequestHandler()
    {
        $obj = new ElasticsearchClient();
        $actual = $obj->getAwsRequestHandler();
        $this->assertInstanceOf(AwsRequestHandler::class, $actual);
    }

    public function testGetAwsDefaultHttpHandler()
    {
        $obj = new ElasticsearchClient();
        $actual = $obj->getAwsDefaultHttpHandler();
        $this->assertInstanceOf(GuzzleHandler::class, $actual);
    }

    public function testIsAwsEndpointWithAwsFlagEnabled()
    {
        $options = [
            'hosts' => [
                'test.abc.com'
            ],
            'aws_host' => true
        ];
        $obj = new ElasticsearchClient();
        $actual = $obj->isAwsEndpoint($options);
        $this->assertTrue($actual);
    }

    public function testIsAwsEndpointWithAwsHost()
    {
        $options = [
            'hosts' => [
                'test.amazonaws.com'
            ]
        ];
        $obj = new ElasticsearchClient();
        $actual = $obj->isAwsEndpoint($options);
        $this->assertTrue($actual);
    }

    public function testIsAwsEndpointWithNonAwsHost()
    {
        $options = [
            'hosts' => [
                'test.abc.com'
            ]
        ];
        $obj = new ElasticsearchClient();
        $actual = $obj->isAwsEndpoint($options);
        $this->assertFalse($actual);
    }

    public function testGetHostsWithoutException()
    {
        $options = [
            'hosts' => [
                'test.amazonaws.com'
            ]
        ];
        $obj = new ElasticsearchClient();
        $actual = $obj->getHosts($options);
        $this->assertEquals($options['hosts'], $actual);
    }

    public function testGetHostsWithException()
    {
        $this->expectException(\InvalidArgumentException::class);
        $options = [];
        $obj = new ElasticsearchClient();
        $obj->getHosts($options);
    }

    public function testGetAccessKeyWithoutException()
    {
        $options = ['access_key' => 'KJHFI8342jhk'];
        $obj = new ElasticsearchClient();
        $actual = $obj->getAccessKey($options);
        $this->assertEquals($options['access_key'], $actual);
    }

    public function testGetAccessKeyWithException()
    {
        $this->expectException(\InvalidArgumentException::class);
        $options = [];
        $obj = new ElasticsearchClient();
        $obj->getAccessKey($options);
    }


    public function testGetSecretKeyWithoutException()
    {
        $options = ['secret_key' => 'FRD64adf321'];
        $obj = new ElasticsearchClient();
        $actual = $obj->getSecretKey($options);
        $this->assertEquals($options['secret_key'], $actual);
    }

    public function testGetSecretKeyWithException()
    {
        $this->expectException(\InvalidArgumentException::class);
        $options = [];
        $obj = new ElasticsearchClient();
        $obj->getSecretKey($options);
    }

    public function testGetRegionWithoutException()
    {
        $options = ['region' => 'ap-southeast-2'];
        $obj = new ElasticsearchClient();
        $actual = $obj->getRegion($options);
        $this->assertEquals($options['region'], $actual);
    }

    public function testGetRegionWithException()
    {
        $this->expectException(\InvalidArgumentException::class);
        $options = [];
        $obj = new ElasticsearchClient();
        $obj->getRegion($options);
    }
}