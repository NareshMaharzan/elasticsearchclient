<?php
namespace Naresh\ElasticSearchClient\Tests;

use Naresh\ElasticSearchClient\EsClient;
use Naresh\ElasticSearchClient\ServiceProvider;
use Orchestra\Testbench\TestCase as OrchestraTestCase;

/**
 * Class TestCase
 * @package Naresh\ElasticSearchClient\Tests
 */
class TestCase extends OrchestraTestCase
{

    /**
     * Load package service provider
     * @param  \Illuminate\Foundation\Application $app
     * @return array
     */
    protected function getPackageProviders(/** @scrutinizer ignore-unused */ $app)
    {
        return [ServiceProvider::class];
    }
    /**
     * Load package alias
     * @param  \Illuminate\Foundation\Application $app
     * @return array
     */
    protected function getPackageAliases(/** @scrutinizer ignore-unused */ $app)
    {
        return [
            'EsClient' => EsClient::class,
        ];
    }
}