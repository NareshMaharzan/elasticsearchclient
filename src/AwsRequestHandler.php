<?php

declare(strict_types = 1);

namespace Naresh\ElasticSearchClient;

use Naresh\ElasticSearchClient\Contracts\AwsRequestHandlerInterface;
use GuzzleHttp\Ring\Future\CompletedFutureArray;
use Aws\Credentials\CredentialsInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\RequestInterface;
use Aws\Signature\SignatureInterface;
use Aws\Credentials\Credentials;
use Aws\Signature\SignatureV4;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Uri;

/**
 * Class AwsRequestHandler
 * @package Naresh\ElasticSearchClient
 */
class AwsRequestHandler implements AwsRequestHandlerInterface
{
    /**
     * @var CredentialsInterface
     */
    protected $credentials;

    /**
     * @var SignatureInterface
     */
    protected $signature;

    /**
     * @inheritdoc
     */
    public function setCredentials(string $accessKey, string $secretKey)
    {
        $this->credentials = new Credentials($accessKey, $secretKey);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getCredentials()
    {
        return ($this->credentials);
    }

    /**
     * @inheritdoc
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * @inheritdoc
     */
    public function setSignature(string $region)
    {
        $this->signature = new SignatureV4('es', $region);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getHandler(callable $handler)
    {
        return function ($request) use ($handler) {
            $psrRequest = $this->getRequest($request);
            $signedResponse = $this->signRequest($psrRequest);
            $response = $this->sendRequestToAws($handler, $signedResponse);

            return $this->getRingPhpResponse($psrRequest, $response);
        };
    }

    /**
     * @inheritdoc
     */
    public function getRequest(array $request)
    {
        $request['headers']['host'][0] = parse_url($request['headers']['host'][0], PHP_URL_HOST);
        $host = $request['headers']['host'][0];
        $method = $request['http_method'];
        $scheme = $request['scheme'];
        $uri = $request['uri'];
        $body = $request['body'];
        $headers = $request['headers'];

        return new Request($method, $this->getUri($uri, $scheme, $host), $headers, $body);
    }

    /**
     * @inheritdoc
     */
    public function getUri($uri, $scheme, $host)
    {
        return (new Uri($uri))
            ->withScheme($scheme)
            ->withHost($host);
    }

    /**
     * @inheritdoc
     */
    public function signRequest(Request $request)
    {
        return $this->getSignature()->signRequest($request, $this->getCredentials());
    }

    /**
     * @inheritdoc
     */
    public function sendRequestToAws(callable $handler, RequestInterface $signedRequest)
    {
        $response = $handler($signedRequest)->then()->wait();

        return $response;
    }

    /**
     * @inheritdoc
     */
    public function getRingPhpResponse(RequestInterface $request, ResponseInterface $response)
    {
        return new CompletedFutureArray([
            'status' => $response->getStatusCode(),
            'headers' => $response->getHeaders(),
            'body' => $response->getBody()->detach(),
            'transfer_stats' => ['total_time' => 0],
            'effective_url' => (string)$request->getUri()
        ]);
    }
}