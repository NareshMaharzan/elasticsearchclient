<?php

namespace Naresh\ElasticSearchClient;

use Illuminate\Support\Facades\Facade;
use Elasticsearch\Client;

/**
 * Class EsClient Facade
 * @package Naresh\ElasticSearchClient
 *
 *
 * @method getClient(array $options): Client
 * @uses ElasticsearchClient::getClient(): Client
 *
 */
class EsClient extends Facade
{
    /**
     * Get facade instance.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'EsClient';
    }
}