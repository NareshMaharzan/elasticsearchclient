<?php

declare(strict_types=1);

namespace Naresh\ElasticSearchClient;

use Naresh\ElasticSearchClient\Contracts\ElasticSearchClientInterface;
use Elasticsearch\ClientBuilder;
use InvalidArgumentException;


/**
 * Class ElasticsearchClient
 * @package Naresh\ElasticSearchClient
 */
class ElasticsearchClient implements ElasticSearchClientInterface
{

    /**
     * @inheritdoc
     */
    public function getClient(array $options)
    {
        if ($this->isAwsEndpoint($options)) {
            return $this->getAwsEsClient($options);
        }

        return $this->getNormalClient($options);
    }


    /**
     * @inheritdoc
     */
    public function getNormalClient(array $options)
    {
        $clientBuilder = $this->getClientBuilder()->setHosts($this->getHosts($options));
        return $clientBuilder->build();
    }

    /**
     * @inheritdoc
     */
    public function getAwsEsClient(array $options)
    {
        $awsEs = $this->getAwsRequestHandler()
            ->setCredentials($this->getAccessKey($options), $this->getSecretKey($options))
            ->setSignature($this->getRegion($options));

        $awsHandler = $awsEs->getHandler($this->getAwsDefaultHttpHandler());
        $clientBuilder = $this->getClientBuilder()
            ->setHandler($awsHandler)
            ->setHosts($this->getHosts($options));
        return $clientBuilder->build();
    }

    /**
     * @inheritdoc
     */
    public function getClientBuilder()
    {
        return ClientBuilder::create();
    }

    /**
     * @inheritdoc
     */
    public function getAwsRequestHandler()
    {
        return new AwsRequestHandler();
    }

    /**
     * @inheritdoc
     */
    public function getAwsDefaultHttpHandler()
    {
        return \Aws\default_http_handler();
    }


    /**
     * @inheritdoc
     */
    public function isAwsEndpoint(array $options)
    {
        $hosts = $options['hosts'];
        $awsHost = $options['aws_host'] ?? false;
        if ($awsHost) {
            return true;
        }
        foreach ($hosts as $endpoint) {
            if (strstr($endpoint, 'amazonaws') !== false) {
                return true;
            }
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function getHosts(array $options)
    {
        $hosts = $options['hosts'] ?? null;
        if (empty($hosts)) {
            throw new InvalidArgumentException("Invalid host(s)");
        }
        return is_array($hosts) ? $hosts : [$hosts];
    }

    /**
     * @inheritdoc
     */
    public function getAccessKey(array $options)
    {
        $accessKey = $options['access_key'] ?? null;
        if (empty($accessKey) || !is_string($accessKey)) {
            throw new InvalidArgumentException("Invalid access key");
        }
        return $accessKey;
    }

    /**
     * @inheritdoc
     */
    public function getSecretKey(array $options)
    {
        $secretKey = $options['secret_key'] ?? null;
        if (empty($secretKey) || !is_string($secretKey)) {
            throw new InvalidArgumentException("Invalid secret key");
        }
        return $secretKey;
    }

    /**
     * @inheritdoc
     */
    public function getRegion(array $options)
    {
        $region = $options['region'] ?? null;
        if (empty($region) || !is_string($region)) {
            throw new InvalidArgumentException("Invalid region");
        }
        return $region;
    }
}

