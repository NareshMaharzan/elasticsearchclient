<?php

namespace Naresh\ElasticSearchClient\Contracts;

use Aws\Signature\SignatureInterface;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Interface AwsEsInterface
 */
interface AwsRequestHandlerInterface
{
    /**
     * Set AWS credentials
     * @param string $accessKey
     * @param string $secretKey
     * @return mixed
     */
    public function setCredentials(string $accessKey, string $secretKey);

    /**
     * Get AWS Credentials
     * @return mixed
     */
    public function getCredentials();

    /**
     * Get signature
     * @return SignatureInterface
     */
    public function getSignature();

    /**
     * Set signature
     * @param string $region
     * @return mixed
     */
    public function setSignature(string $region);

    /**
     * Get AWS handler
     * @param callable $handler
     * @return mixed
     */
    public function getHandler(callable $handler);

    /**
     * Get request
     * @param array $request
     * @return mixed
     */
    public function getRequest(array $request);

    /**
     * Get URI
     * @param $uri
     * @param $scheme
     * @param $host
     * @return mixed
     */
    public function getUri($uri, $scheme, $host);

    /**
     * Sign request
     * @param Request $request
     * @return mixed
     */
    public function signRequest(Request $request);

    /**
     * Send request to AWS
     * @param callable $handler
     * @param RequestInterface $signedRequest
     * @return mixed
     */
    public function sendRequestToAws(callable $handler, RequestInterface $signedRequest);

    /**
     * Get Ring PHP response
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @return mixed
     */
    public function getRingPhpResponse(RequestInterface $request, ResponseInterface $response);
}