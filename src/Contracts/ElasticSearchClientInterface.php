<?php

namespace Naresh\ElasticSearchClient\Contracts;

use Elasticsearch\ClientBuilder;
use Elasticsearch\Client;


/**
 * Interface ElasticSearchClientInterface
 * @package Naresh\ElasticSearchClient\Contracts
 */
interface ElasticSearchClientInterface
{
    /**
     * Get Elasticsearch Client
     *
     * @param array $options
     * @return Client
     */
    public function getClient(array $options);

    /**
     * Get normal elasticserach client
     * @param array $options
     * @return Client
     */
    public function getNormalClient(array $options);

    /**
     * Get AWS elasticsearch client
     * @param array $options
     * @return Client
     */
    public function getAwsEsClient(array $options);

    /**
     * Check if the host is elasticsearch endpoint
     * @param array $options
     * @return bool
     */
    public function isAwsEndpoint(array $options);

    /**
     * Get hosts
     * @param array $options
     * @return array
     */
    public function getHosts(array $options);

    /**
     * Get access key
     * @param array $options
     * @return string
     */
    public function getAccessKey(array $options);


    /**
     * Get secret key
     * @param array $options
     * @return string
     */
    public function getSecretKey(array $options);

    /**
     * Get region
     * @param array $options
     * @return string
     */
    public function getRegion(array $options);

    /**
     * Get Client builder instance
     * @return ClientBuilder
     */
    public function getClientBuilder();

    /**
     * Get aws request handler
     * @return AwsRequestHandlerInterface
     */
    public function getAwsRequestHandler();

    /**
     * Get AWS default http handler
     * @return callable
     */
    public function getAwsDefaultHttpHandler();

}