<?php

namespace Naresh\ElasticSearchClient;

use Illuminate\Support\ServiceProvider as Provider;

/**
 * Class ServiceProvider
 * @package Naresh\ElasticSearchClient
 */
class ServiceProvider extends Provider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('EsClient', ElasticsearchClient::class);
    }
}